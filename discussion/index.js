// console.log("Hello Wednesday!");

// [SECTION] While Loop
/*
    - A while loop takes in an expression/condition
    - Expressions are any unit of code that can be evaluated to a value
    - If the condition evaluates to true, the statements inside the code block will be executed
    - A statement is a command that the programmer gives to the computer
    - A loop will iterate a certain number of times until an expression/condition is met
    - "Iteration" is the term given to the repetition of statements
    - Syntax
        while(expression/condition){
			statement
        }
*/

let count = 5;

while(count !== 0) {
    // While: 5 -- 1st Iteration
    // While: 4 -- 2nd Iteration
    // While: 3 -- 3nd Iteration
    // While: 2 -- 4nd Iteration
    // While: 1 -- 5th Iteration

	console.log("While: " + count);

    // count = 4
    // count = 3
    // count = 2
    // count = 1
    // count = 0
    count--;
};

// [SECTION] Do-While Loop
/*
    - A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
    - Syntax
        do {
            statement
        } while (expression/condition)
*/

// prompt takes an input as data type "String"
// "Number" function works similarly with the "parseInt" function
let number = Number(prompt("Give me a number")); // 4

do {
    // Do While: 4
    // Do While: 5
    // Do While: 6
    console.log("Do While: " + number);

    // number = 5
    // number = 6
    // number = 7
    number += 1;

} while(number < 10);

// [SECTION] For Loop

/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

// value = 0
// value = 1
// value = 2
// value = 3
// value = 4
// value = 5
// value = 6
// value = 7
// value = 8
for(let value = 0; value <= 7; value++) {

    // 0
    // 1
    // 2
    // 3
    // 4
    // 5
    // 6
    // 7
    console.log(value);
}

let string = "Wednesday";

// Characters in strings may be counted using the .length property
// Strings are special compared to other data types that it has access to functions and other pieces of information another primitive data type might not have
console.log(string.length);

// Accessing elements of a string
// Individual characters of a string may be accessed using it's index number
// The first character in a string corresponds to the number 0, the next is 1 up to the nth number
console.log(string[0]);
console.log(string[1]);
console.log(string[5]);

for(let x = 0; x < string.length; x++) {

    console.log(string[x]);
}

let myName = "HILLARY";

for(let i = 0; i < myName.length; i++) {

    // If the character of your name is a vowel letter, instead of displaying the character, display number "3"
    // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match

    if(myName[i].toLowerCase() === 'a' || 'e' || myName[i].toLowerCase == 'i' || myName[i].toLowerCase == 'o' || myName[i].toLowerCase == 'u' ) {
        console.log(3);
    }
    else {
        console.log(myName[i]);
    }
};

// [SECTION] Continue and Break Statements
/*
    - The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
    - The "break" statement is used to terminate the current loop once a match has been found
*/

for(let num = 0; num <= 20; num++) {

    if(num % 2 === 0) {
        continue;
    }
    if(num > 10) {
        break;
    }
    console.log("continue and break: " + num);
};

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
    
    // The current letter is printed out based on it's index
    console.log(name[i]);

    // If the vowel is equal to a, continue to the next iteration of the loop
    if (name[i].toLowerCase() === "a") {
        console.log("Continue to the next iteration");
        continue;
    }

    // If the current letter is equal to d, stop the loop
    if (name[i] == "d") {
        break;
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
}

if  the name is equal to the letter a, change the letter to y,

    if 